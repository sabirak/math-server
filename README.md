## High performance test server

To build using cmake run 
```
mkdir build
cd build 
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

This creates 3 executables to run the server client benchmark and the tests
```
./math_server NUM_THREADS
./math_client NUM_CLIENTS
./gtest_tests
```

Too allow for more connections to the server run 
```
ulimit -n 2048
```
to allow 2048 open file descriptors