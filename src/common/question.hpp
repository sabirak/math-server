#ifndef MATH_SERVER_QUESTION_HPP
#define MATH_SERVER_QUESTION_HPP

#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <json.hpp>

#include "../config.h"

using nlohmann::json;

namespace common {

json generate_sum_question();
json build_sum_question(std::vector<numb_t>);

}
#endif //MATH_SERVER_QUESTION_HPP
