#include "question.hpp"

#include <ctime>

namespace common {

json generate_sum_question() {
    srand(static_cast<numb_t>(time(nullptr)));
    auto count = static_cast<unsigned int>(rand() % MAX_NUMBER_COUNT + 1);
    std::vector<numb_t> numbers;
    for (auto i = 0; i < count; i++) {
        numbers.emplace_back(static_cast<numb_t>(rand() + 1));
    }
    return build_sum_question(numbers);
}
json build_sum_question(std::vector<numb_t> numbers) {
    json json;
    json[ACTION_STR] = ACTION_SUM;
    json[DATA_STR] = numbers;
    return json;
}
}