#include <iostream>
#include <thread>
#include <vector>
#include <random>
#include <cassert>

#include "client.hpp"
#include "../utils.hpp"
#include "../math/math.hpp"

int request_server() {
    client::Client client;
    client.initialize();
    for (auto i = 0; i < 1000; i++) {
        auto q = common::generate_sum_question();
        client.send_question(q);
        auto answer_json = client.read_answer();
        auto answer = answer_json[RESULT_STR];
        auto truth = math::sum(q[DATA_STR]);
        assert(answer == truth);
        #ifndef NDEBUG
        std::cout << "Correct answer\n";
        #endif
        utils::random_sleep(0, 10);
    }
    return 0;
}

int main(int argc, char **argv) {
    int nthreads = 2;
    if (argc == 2) {
        nthreads = std::stoi(argv[1]);
    }
    std::cout << "Starting";
    std::cout << " " << nthreads << " clients\n";
    std::vector<std::thread> threads;
    for (auto i = 0; i < nthreads; i++) {
        threads.emplace_back(std::thread(request_server));
    }
    for (auto &t:threads) {
        t.join();
    }

    return 0;
}