#include "client.hpp"

#include <cstdio>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctime>
#include <json.hpp>

using nlohmann::json;
namespace client {

int Client::initialize() {
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(SERVER_PORT);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if (inet_pton(AF_INET, SERVER_IP, &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr))
        < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
    return 0;
}

int Client::send_question(const json& json) {
    auto r_str = json.dump();
    send(sock, r_str.c_str(), r_str.size(), 0);
    return 0;
}

json Client::read_answer() {
    const size_t buffer_size = 1024;
    auto buffer = static_cast<char*>(calloc(buffer_size, 1));
    read(sock, buffer, buffer_size);
    auto json = json::parse(buffer);
    free(buffer);
    return json;
}

int Client::close_socket() {
    return close(sock);
}
}
