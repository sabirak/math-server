#ifndef MATH_SERVER_CLIENT_HPP
#define MATH_SERVER_CLIENT_HPP

#include <cstdlib>
#include <netinet/in.h>
#include "../common/question.hpp"

namespace client {

class Client {
 public:

  Client() = default;

  ~Client(){
      close_socket();
  }

  int initialize();
  int send_question(const nlohmann::json&);
  json read_answer();
  int close_socket();

 private:
  int sock{};
  sockaddr_in serv_addr{};
};

}
#endif //MATH_SERVER_CLIENT_HPP
