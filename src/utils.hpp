#ifndef MATH_SERVER_UTILS_HPP
#define MATH_SERVER_UTILS_HPP
namespace utils {
void reverse_bytes(void *start, int size);
void random_sleep(int from, int to);
}
#endif //MATH_SERVER_UTILS_HPP
