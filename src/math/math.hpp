#ifndef MATH_SERVER_MATH_HPP
#define MATH_SERVER_MATH_HPP
#include "../config.h"
#include "../common/question.hpp"
namespace math {
answer_t sum(std::vector<numb_t> numbers);
}
#endif //MATH_SERVER_MATH_HPP
