#include <numeric>
#include "../config.h"
#include "../common/question.hpp"

namespace math {

answer_t sum(std::vector<numb_t> numbers) {
    answer_t sum = 0;
    for(auto n : numbers){
        sum+=n;
    }
    return sum;
}
}
