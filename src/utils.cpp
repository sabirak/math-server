#include <algorithm>
#include <random>
#include <thread>
namespace utils {
void reverse_bytes(void *start, int size) {
    char *istart = static_cast<char *>(start), *iend = istart + size;
    std::reverse(istart, iend);
}

void random_sleep(int from, int to) {
    std::mt19937_64 eng{std::random_device{}()};
    std::uniform_int_distribution<> dist{from, to};
    std::this_thread::sleep_for(std::chrono::milliseconds{dist(eng)});
}

}