#include <gtest/gtest.h>
#include <mutex>
#include <thread>

#include "../server/server.hpp"
#include "../client/client.hpp"

using server::Server;
using client::Client;

void run_server_once(volatile bool *server_started);
void run_server(volatile bool *server_started, int total_num_clients);
int run_client();

TEST(SERVER, SERVER_MAX_ANSWER_Test) {
    volatile bool server_started;
    server_started = 0;
    std::thread t_server(run_server_once, &server_started);
    // wait for the server
    while (!server_started) {
        std::this_thread::sleep_for(std::chrono::milliseconds{1});
    }
    Client client;
    if (client.initialize() >= 0) {
        std::vector<numb_t> numbers =
            {UINT32_MAX, UINT32_MAX, UINT32_MAX, UINT32_MAX, UINT32_MAX};
        auto q = common::build_sum_question(numbers);
        client.send_question(q);
        auto answer = client.read_answer();
        EXPECT_EQ(math::sum(q[DATA_STR]), answer[RESULT_STR]);
    } else {
        EXIT_FAILURE;
    }
    t_server.join();
}

TEST(SERVER, SERVER_1_Number_Test) {
    volatile bool server_started;
    server_started = 0;
    std::thread t_server(run_server_once, &server_started);
    // wait for the server
    while (!server_started) {
        std::this_thread::sleep_for(std::chrono::milliseconds{1});
    }
    Client client;
    if (client.initialize() >= 0) {
        std::vector<numb_t> numbers = {0, 0, 0, 0, 0};
        auto q = common::build_sum_question(numbers);
        client.send_question(q);
        auto answer = client.read_answer();
        EXPECT_EQ(math::sum(q[DATA_STR]), answer[RESULT_STR]);
    } else {
        EXIT_FAILURE;
    }
    t_server.join();
}

TEST(SERVER, SERVER_6_Number_Test) {
    volatile bool server_started;
    server_started = 0;
    std::thread t_server(run_server_once, &server_started);
    // wait for the server
    while (!server_started) {
        std::this_thread::sleep_for(std::chrono::milliseconds{1});
    }
    Client client;
    if (client.initialize() >= 0) {
        std::vector<numb_t> numbers = {0, 0, 0, 0, 0};
        auto q = common::build_sum_question(numbers);
        client.send_question(q);
        auto answer = client.read_answer();
        EXPECT_EQ(math::sum(q[DATA_STR]), answer[RESULT_STR]);
    } else {
        EXIT_FAILURE;
    }
    t_server.join();
}

TEST(SERVER, SERVER_MULTIPLE_CLIENTS_Test) {
    volatile bool server_started;
    server_started = 0;
    int nthreads = 100;
    std::thread t_server(run_server, &server_started, nthreads);
    // wait for the server
    while (!server_started) {
        std::this_thread::sleep_for(std::chrono::milliseconds{1});
    }
    std::vector<std::thread> threads;
    for (auto i = 0; i < nthreads; i++) {
        threads.emplace_back(std::thread(run_client));
    }
    for (auto &t:threads) {
        t.join();
    }
    t_server.join();
}

void run_server_once(volatile bool *server_started) {
    Server server;
    *server_started = 1;
    auto client_socket = server.accept_new_connection();
    auto question = server.read_question(client_socket);
    auto answer = server::compute_answer(question);
    server.send_answer(answer, client_socket);
}

std::mutex socket_mutex;

int serve_client(server::Server *server, int num_clients) {

    std::thread::id this_id = std::this_thread::get_id();
    int client_socket;
    for (int i = 0; i < num_clients; i++) {
        {
            std::lock_guard<std::mutex> lock(socket_mutex);
            client_socket = server->accept_new_connection();
        }
        auto q = server->read_question(client_socket);
        while (q[QUESTION_EMPTY_STR] == nullptr) {
            auto answer = server::compute_answer(q);
            server->send_answer(answer, client_socket);
            q = server->read_question(client_socket);
        }
    }
}

void run_server(volatile bool *server_started, int total_num_clients) {
    Server server;
    std::vector<std::thread> threads;
    for (auto i = 0; i < total_num_clients/2; i++) {
        threads.emplace_back(std::thread(serve_client, &server, 2));
    }
    *server_started = 1;
    for (auto &t:threads) {
        t.join();
    }
}

int run_client() {
    client::Client client;
    client.initialize();
    for (auto i = 0; i < 10; i++) {
        auto q = common::generate_sum_question();
        client.send_question(q);
        auto answer = client.read_answer();
        EXPECT_EQ(math::sum(q[DATA_STR]), answer[RESULT_STR]);
    }
    utils::random_sleep(0, 10);
}