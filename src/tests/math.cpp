#include <gtest/gtest.h>

#include "../math/math.hpp"
#include "../utils.hpp"

TEST(MATH, MATH_SUM_MAX_INT_Test) {
    std::vector<numb_t>numbers =
        {UINT32_MAX, UINT32_MAX, UINT32_MAX, UINT32_MAX, UINT32_MAX};
    auto a = math::sum(numbers);
    auto sum = UINT32_MAX * 5l;
    EXPECT_EQ(sum, a);
}
