#ifndef MATH_SERVER_SERVER_HPP
#define MATH_SERVER_SERVER_HPP

#include <cstddef>

#include <unistd.h>
#include <cstdio>
#include <sys/socket.h>
#include <cstdlib>
#include <netinet/in.h>

#include "../config.h"
#include "../common/question.hpp"
#include "../common/optional.hpp"

using common::Optional;

namespace server {

class Server {
 public:
  Server() {
      initialize_socket();
  };

  ~Server(){
      close(server_fd);
  }

  int accept_new_connection();

  nlohmann::json read_question(int);
  int send_answer(nlohmann::json, int);

 private:
  sockaddr_in address{};
  int server_fd{};

  void initialize_socket();
};


nlohmann::json compute_answer(const nlohmann::json &_json);
}
#endif //MATH_SERVER_SERVER_HPP
