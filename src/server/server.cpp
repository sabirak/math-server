#include <json.hpp>
#include "server.hpp"
#include "../math/math.hpp"

using nlohmann::json;

namespace server {

/**
 * @brief create a listening socket
 */
void Server::initialize_socket() {
    int opt = 1;

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                   &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(SERVER_PORT);

    if (bind(server_fd, (struct sockaddr *) &address,
             sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    int backlog = 128;
    if (listen(server_fd, backlog) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
}

/**
 * @brief wait for a new client
 * @return client socket
 */
int Server::accept_new_connection() {
    int addrlen = sizeof(address);
    int client_socket;
    if ((client_socket = accept(server_fd, (struct sockaddr *) &address,
                                (socklen_t *) &addrlen)) < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    return client_socket;
}

/**
 * @brief read and parse a new question
 * @param client_socket
 * @return parsed question in json format
 */
json Server::read_question(int client_socket) {
    const size_t buffer_size = 1024;
    auto buffer = static_cast<char *>(calloc(buffer_size, 1));

    ssize_t bytes_read;
    if ((bytes_read = read(client_socket, buffer, buffer_size)) == 0) {
        json json;
        json[QUESTION_EMPTY_STR] = true;
        return json;
    }
    std::string b_str = buffer;
    auto json = json::parse(b_str);
    free(buffer);
    return json;
}

int Server::send_answer(nlohmann::json json, int client_socket) {
    auto a_str = json.dump();
    send(client_socket, a_str.c_str(), a_str.size(), 0);
    return 0;
}

/**
 * @brief compute answer for a given question
 * @param _json question
 * @return  answer
 */
nlohmann::json compute_answer(const nlohmann::json &_json) {
    auto action = _json[ACTION_STR];
    json a_json;
    if (action == ACTION_SUM) {
        auto data = _json[DATA_STR];
        if (data.size() > 5) {
            a_json[RESULT_STR] = "Too many numbers";
        } else {
            a_json[RESULT_STR] = math::sum(data);
        }
    } else {
        a_json[RESULT_STR] = "Unknown action specified";
    }
    return a_json;
}
}