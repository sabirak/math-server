#include <iostream>
#include <thread>
#include <mutex>
#include <vector>

#include "server.hpp"
#include "../math/math.hpp"

std::mutex socket_mutex;

/*
 * Wait for a client to connect
 * Serve all requests of connected client
 */
int serve_client(server::Server *server) {
    for (;;) {
        int client_socket;
        {
            std::lock_guard<std::mutex> lock(socket_mutex);
            client_socket = server->accept_new_connection();
        }
        auto q = server->read_question(client_socket);
        while (q[QUESTION_EMPTY_STR] == nullptr)  {
            auto answer = server::compute_answer(q);
            server->send_answer(answer, client_socket);
            q = server->read_question(client_socket);
        }
        close(client_socket);
    }
}

int main(int argc, char **argv) {

    server::Server server;
    int nthreads = 20;
    if (argc == 2) {
        nthreads = std::stoi(argv[argc - 1]);
    }
    std::cout << "Starting server";
    std::cout << " with " << nthreads << " threads\n";
    std::vector<std::thread> threads;
    // create thread pool of nthreads
    for (auto i = 0; i < nthreads; i++) {
        threads.emplace_back(std::thread (serve_client, &server));
    }

    for (auto &t:threads){
        t.join();
    }
    return 0;
}