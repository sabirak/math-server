#ifndef MATH_SERVER_CONFIG_H
#define MATH_SERVER_CONFIG_H

#define SERVER_PORT 8046
#define SERVER_IP "127.0.0.1"

#define numb_t unsigned int
#define answer_t unsigned long
#define MAX_NUMBER_COUNT 5

#define ACTION_SUM "sum"
#define ACTION_STR "action"
#define DATA_STR "data"
#define RESULT_STR "result"
#define QUESTION_EMPTY_STR "question_empty"

#endif //MATH_SERVER_CONFIG_H
